import React from 'react';
import { Link } from 'react-router-dom';

export const Layout: React.FC = ({ children }) => {
  return (
    <div className="m-0 p-0">
      <div>
        <ul>
          <li>
            <Link to="/">Inicio</Link>
          </li>
          <li>
            <Link to="/memory-game">Juego de memoria</Link>
          </li>
        </ul>

        <hr />
      </div>
      <div className="mx-32 mt-5">{children}</div>
    </div>
  );
};
