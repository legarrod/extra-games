export { default as ModuleHome } from './Home';
export { default as MemoryGame } from './MemoryGame';
export { default as PageNotFaund } from './404';
