import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Layout } from 'components/Layout/Layout';
import { routes } from './routes';
function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          {routes.map(({ path, Component, exact = false }) => (
            <Route exact={exact} path={path} component={Component} key={path} />
          ))}
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
