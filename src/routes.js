import { ModuleHome, MemoryGame, PageNotFaund } from 'pages';

export const routes = [
  {
    path: '/',
    label: 'Home',
    Component: ModuleHome,
    exact: true,
  },
  {
    path: '/memory-game',
    label: 'Memory game',
    Component: MemoryGame,
    exact: true,
  },
  {
    path: '*',
    label: 'Error Page',
    Component: PageNotFaund,
    exact: true,
  },
];
