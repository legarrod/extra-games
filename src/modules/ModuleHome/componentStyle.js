import tw, { styled } from 'twin.macro';

export const StyledInput = styled.input(({ hasBorder }) => [
  `color: black;`,
  hasBorder && tw`border-2 border-red-500 rounded-md p-1 text-blue-900`,
]);
