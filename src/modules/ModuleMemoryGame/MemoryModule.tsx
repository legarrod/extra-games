import { useState } from 'react';
import { Card } from './components/Card/Card';
import { iconsCards } from './constans';

export interface IDataIcon {
  icon: string;
  isVisible: boolean;
  index?: number;
}

export interface IMemoryModuleProps {
  lucho?: 'primary' | 'default';
}

const MemoryModule: React.FC<IMemoryModuleProps> = () => {
  const [icons, setIcons] = useState(iconsCards.concat(iconsCards));
  const [imageSelected, setImageSelected] = useState<IDataIcon>();

  const handlerClickCard = (animal: IDataIcon, indexSelected: number) => {
    const iconsChanged = icons.map((icon, index) => {
      if (index === indexSelected) {
        !imageSelected && setImageSelected({ ...icon, index });
        return { ...icon, isVisible: true };
      }

      return icon;
    });

    setIcons(iconsChanged);

    if (imageSelected) {
      if (animal.icon === imageSelected.icon) {
        console.log('correcto y desbloquear');
      } else {
        const undoIcons = icons.map((icon, index) => {
          if (index === indexSelected || index === imageSelected.index) {
            return { ...icon, isVisible: false };
          }

          return icon;
        });

        setTimeout(() => {
          setIcons(undoIcons);
        }, 1000);
      }

      setImageSelected(undefined);
    }
  };
  console.table(imageSelected);

  return (
    <div>
      <div className='flex flex-wrap justify-around'>
        {icons.map((icon: IDataIcon, index: number) => (
          <Card
            key={index}
            icon={icon}
            index={index}
            handlerClickCard={handlerClickCard}
          />
        ))}
      </div>
    </div>
  );
};

export default MemoryModule;
