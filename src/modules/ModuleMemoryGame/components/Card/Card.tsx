import { useState } from 'react';
import { IDataIcon } from '../../MemoryModule';

export interface IDataCard {
  icon: IDataIcon;
  index: number;
  handlerClickCard: any;
}

export const Card: React.FC<IDataCard> = ({
  icon,
  index,
  handlerClickCard,
}) => {
  const [pendingRotate] = useState<string>('❓');
  return (
    <button onClick={() => handlerClickCard(icon, index)}>
      <p className='text-7xl p-4 border-2 border-blue-500 rounded-md m-2'>
        {icon.isVisible ? icon.icon : pendingRotate}
      </p>
    </button>
  );
};
